import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import './Login.css'
import { auth } from './firebase'
import { Email } from '@material-ui/icons'

function Login() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const history = useHistory()

    const login = (event) => {
        event.preventDefault()
        auth.signInWithEmailAndPassword(email, password)
        .then((auth) => {
            history.push('/')
        })
        .catch(err => {
            alert(err.message)
        })
    }

    const register = (event) => {
        event.preventDefault()
        auth.createUserWithEmailAndPassword(email, password)
        .then((auth) => {
            history.push('/')
        })
        .catch(err => {
            alert(err.message)
        })

    }
    return ( 
        <div className="login">
            <Link to="/">
                <img
                    className="login__logo"
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Amazon_logo.svg/1024px-Amazon_logo.svg.png"
                />
            </Link>
            <div className="login__container">
                <h1>Sign In</h1>
                <form>
                    <h5>Email</h5>
                    <input type="email" onChange={event => setEmail(event.target.value)} value={email} />
                    <h5>Password</h5>
                    <input type="password" onChange={event => setPassword(event.target.value)} value={password} />
                    <button onClick={login} type="submit" className="login__signInButton">Sign In</button>
                    <p>By signing in, you agree to Amazon's Conditions Use & Sale. Please see our Privacy Notice, our Cookies Notice and our Interests based Ads Notice</p>
                    <button onClick={register} className="login__registerButton">Create Your Amazon Account</button>
                </form>
            </div>
        </div>
    )
}

export default Login